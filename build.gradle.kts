plugins {
  `maven-publish`
  alias(libs.plugins.jvm)
  alias(libs.plugins.spotless)
}

group = "com.gitee.ixtf"

version = "3.2.8"

dependencies {
  api(platform(libs.bom))
  api("com.gitee.ixtf:core")
  api("org.apache.poi:poi")
  api("org.apache.poi:poi-ooxml")
  api("org.apache.poi:poi-scratchpad")

  testImplementation(platform(libs.junit.bom))
  testImplementation("org.junit.jupiter:junit-jupiter")
  testImplementation(libs.kotlin.test)
  testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

java {
  toolchain { languageVersion.set(JavaLanguageVersion.of(21)) }
  withJavadocJar()
  withSourcesJar()
}

kotlin {
  jvmToolchain(21)
  compilerOptions {
    freeCompilerArgs.add("-Xjsr305=strict")
    freeCompilerArgs.add("-Xemit-jvm-type-annotations")
    freeCompilerArgs.add("-opt-in=kotlin.ExperimentalStdlibApi")
    freeCompilerArgs.add("-Xuse-experimental=kotlin.Experimental")
  }
}

tasks {
  test { useJUnitPlatform() }
  wrapper {
    gradleVersion = "8.9"
    distributionType = Wrapper.DistributionType.ALL
  }
}

publishing {
  publications {
    create<MavenPublication>("mavenJava") {
      from(components["java"])
      versionMapping {
        usage("java-api") { fromResolutionOf("runtimeClasspath") }
        usage("java-runtime") { fromResolutionResult() }
      }
    }
  }
}

spotless {
  java {
    target("**/java/**/*.java", "**/kotlin/**/*.java")
    targetExclude("**/generated/**", "**/generated_tests/**")
    googleJavaFormat()
    formatAnnotations()
    trimTrailingWhitespace()
    endWithNewline()
  }
  kotlin {
    target("**/java/**/*.kt", "**/kotlin/**/*.kt")
    targetExclude("**/generated/**", "**/generated_tests/**")
    ktfmt()
    trimTrailingWhitespace()
    endWithNewline()
  }
  kotlinGradle {
    target("*.gradle.kts", "additionalScripts/*.gradle.kts")
    ktfmt()
    trimTrailingWhitespace()
    endWithNewline()
  }
  format("styling") {
    target("**/resources/**/*.graphql", "**/resources/**/*.graphqls")
    prettier()
    trimTrailingWhitespace()
    endWithNewline()
  }
}
