package com.gitee.ixtf.poi

import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.xssf.usermodel.XSSFWorkbook

object Jpoi {
  fun wb(): Workbook = XSSFWorkbook()
}
